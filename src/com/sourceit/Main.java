package com.sourceit;

import java.util.*;
import java.util.function.Consumer;

public class Main {

    public static void main(String[] args) {
        long start = System.currentTimeMillis();

        LinkedList<User> list = new LinkedList<>();

        for (int i = 0; i < 100; i++) {
            list.add(0, new User("Vasya", i));
        }

        long add = System.currentTimeMillis() - start;

        Iterator<User> iterator = list.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().age > 10) {
                iterator.remove();
            }
        }

        long remove = System.currentTimeMillis() - start - add;




        Collections.sort(list);

        for (User user : list) {
            System.out.println(user);
        }

        Collections.sort(list, new Sort());

        System.out.println();

        for (User user : list) {
            System.out.println(user);
        }

    }

    static class Sort implements Comparator<User>{
        @Override
        public int compare(User user1, User user2) {
            if (user1.age - user2.age != 0) {
                return user1.age - user2.age;
            } else {
                return user1.name.compareTo(user2.name);
            }
        }
    }

    static class User implements Comparable<User> {
        String name;
        int age;

        public User(String name, int age) {
            this.name = name;
            this.age = age;
        }

        @Override
        public int compareTo(User user) {
            if (name.compareTo(user.name) != 0) {
                return name.compareTo(user.name);
            } else {
                return age - user.age;
            }
        }

        @Override
        public String toString() {
            return "User{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }
}
